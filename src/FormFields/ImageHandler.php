<?php

namespace TCG\Voyager\FormFields;

class ImageHandler extends AbstractHandler
{
    protected $viewEdit = 'voyager::formfields.image.edit';
    protected $viewRead = 'voyager::formfields.image.read';
    protected $viewBrowse = 'voyager::formfields.image.browse';
    protected $codename = 'image';

}
